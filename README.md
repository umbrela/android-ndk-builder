# Branches

- master: grabs the latest NDK, need to update SDK url time to time for latest
- sdkmanager-license-bug-temp-fix: same as master, but we need to explicitly set the licenses to workaround a bug
- old-ndk: specifigy the NDK zip, need to update SDK url time to time for latest
- alpine: an expirement to get a smaller image. It's fails when we try to download the NDK.


## Building

Get cmdline-tools version from Dockerfile
Get NDK revision from docker build log or check for latest available ndk and confirm in build log and rename tag.

docker build -t umbrela/android-ndk-builder:cmdtools-6858069-ndk-r22 .