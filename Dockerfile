FROM openjdk:11

RUN apt-get update \
    && apt-get -y --no-install-recommends install file build-essential wget unzip git openssl \
    && apt-get clean

ENV CMDTOOLS_URL        https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip

# ANDROID_HOME is deprecated, use ADNROID_SDK_ROOT instead
ENV ANDROID_SDK_ROOT    /opt/android-sdk-linux
ENV NDK_HOME            $ANDROID_SDK_ROOT/ndk-bundle
ENV PATH                $PATH:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin:$ANDROID_SDK_ROOT/cmdline-tools/tools/bin:$NDK_HOME

# download and unzip sdk, subdirectory structure is important. 
# See https://stackoverflow.com/a/61176718/723980
RUN mkdir -p $ANDROID_SDK_ROOT \
    && wget -q $CMDTOOLS_URL -O cmdtools.zip \
    && unzip cmdtools.zip -d $ANDROID_SDK_ROOT \
    && mv $ANDROID_SDK_ROOT/cmdline-tools $ANDROID_SDK_ROOT/tools \
    && mkdir $ANDROID_SDK_ROOT/cmdline-tools \
    && mv $ANDROID_SDK_ROOT/tools $ANDROID_SDK_ROOT/cmdline-tools/tools \
    && rm -f cmdtools.zip

# accept all licences & install ndk
RUN yes | sdkmanager --licenses \
    && sdkmanager "ndk-bundle"